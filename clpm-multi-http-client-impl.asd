;;;; Clpm-Multi-Http-Client-Impl System Definition
;;;;
;;;; This software is part of clpm-multi-http-client. See README.org for more
;;;; information. See LICENSE for license information.

#-:asdf3.2
(error "Requires ASDF >=3.2")

;; Not necessary, but easier to have when using SLIME.
(in-package :asdf-user)

(defsystem #:clpm-multi-http-client-impl
  :version "0.0.1"
  :description "Implementations of the clpm-multi-http-client interface."
  :license "BSD-2-Clause"
  :pathname "clpm-multi-http-client-impl/"
  :class :package-inferred-system
  :depends-on (#:clpm-multi-http-client-impl/all))
