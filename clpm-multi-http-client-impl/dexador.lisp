;;;; Dexador integration
;;;;
;;;; This software is part of clpm-multi-http-client. See README.org for more
;;;; information. See LICENSE for license information.

(uiop:define-package #:clpm-multi-http-client-impl/dexador
    (:use #:cl
          #:alexandria
          #:flexi-streams
          #:clpm-multi-http-client/connection-pool
          #:clpm-multi-http-client/defs
          #:clpm-multi-http-client/forwarding-input-stream
          #:clpm-multi-http-client/redirects
          #:clpm-multi-http-client/utils)
  (:import-from #:dexador)
  (:export #:dexador-client))

(in-package #:clpm-multi-http-client-impl/dexador)

(defclass dexador-client (http-client)
  ((pool-manager
    :initform nil
    :accessor http-client-pool-manager)))

(defmethod initialize-instance :after ((client dexador-client)
                                       &key (use-connection-pool t)
                                         num-pools
                                         pool-max-size
                                       &allow-other-keys)
  (when use-connection-pool
    (setf (http-client-pool-manager client)
          (apply #'make-instance
                 'pool-manager
                 (append
                  (when num-pools (list :num-pools num-pools))
                  (when pool-max-size (list :pool-max-size pool-max-size)))))))

(defclass dexador-stream (connection-pool-input-stream)
  ()
  (:documentation
   "Wraps Dexador's streams."))

(defmethod close ((ds dexador-stream) &key abort)
  (declare (ignore abort))
  ;; Make sure we've read to the end of the stream.
  (let ((raw-stream (forwarding-input-stream-stream ds)))
    (when raw-stream
      (if (subtypep (stream-element-type raw-stream) 'character)
          (loop
            :until (eql :eof (read-char raw-stream nil :eof)))
          (loop
            :until (eql :eof (read-byte raw-stream nil :eof))))))
  (call-next-method))

(defun headers-to-alist (table)
  (let ((out nil))
    (maphash (lambda (k v)
               (push (cons (make-keyword (uiop:standard-case-symbol-name k)) v)
                     out))
             table)
    out))

(defmethod http-client-request ((client dexador-client) uri
                                &rest args
                                &key (method :get) force-binary
                                  want-stream redirect additional-headers
                                &allow-other-keys)
  (let* ((response-uri-formatter (make-response-uri-formatter uri :quri))
         (uri (ensure-quri uri))
         (pool-manager (http-client-pool-manager client))
         (pool (when pool-manager (manager-get-pool pool-manager uri)))
         (stream (when pool (connection-pool-grab-stream pool))))
    (flet ((add-stream-to-pool (response-uri response-stream)
             (let ((pool (when pool-manager (manager-get-pool pool-manager response-uri))))
               (when pool
                 (connection-pool-add-stream pool response-stream)))))
      (multiple-value-bind
            (response-body response-status-code response-headers response-uri
             response-stream)
          (handler-bind ((dex:http-request-failed #'dex:ignore-and-continue))
            (dex:request
             uri
             :method method
             :stream stream
             :headers additional-headers
             :want-stream want-stream
             :force-binary force-binary
             :max-redirects 0
             :keep-alive pool-manager
             :insecure nil
             :use-connection-pool nil))

        (setf response-headers (headers-to-alist response-headers))

        (when (and (not want-stream) response-stream)
          (unless (add-stream-to-pool response-uri response-stream)
            (ignore-errors (close response-stream))))

        (when (redirect-p response-status-code response-headers redirect)
          (when want-stream
            (when (response-has-body-p response-headers)
              ;; We need to read to the end of the stream before we can return
              ;; it to the pool.
              (if (subtypep (stream-element-type response-body) 'character)
                  (loop
                    :until (eql :eof (read-char response-body nil :eof)))
                  (loop
                    :until (eql :eof (read-byte response-body nil :eof)))))
            (when response-stream
              (unless (add-stream-to-pool response-uri response-stream)
                (ignore-errors (close response-body))
                (ignore-errors (close response-stream)))))
          (return-from http-client-request
            (apply #'handle-redirect client uri response-status-code response-headers
                   :redirect (if (numberp redirect) (1- redirect) redirect)
                   args)))

        (return-from http-client-request
          (values
           (if want-stream
               (make-instance 'dexador-stream
                              :stream (unless (eql response-body response-stream)
                                        response-body)
                              :closedp (eql response-body response-stream)
                              :close-fn (when pool-manager
                                          (let ((already-closed-p nil))
                                            (lambda (abort)
                                              (when (and response-stream (not already-closed-p))
                                                (setf already-closed-p t)
                                                (unless (add-stream-to-pool response-uri response-stream)
                                                  (ignore-errors (close response-body :abort abort))
                                                  (ignore-errors (close response-stream :abort abort))))))))
               response-body)
           response-status-code
           response-headers
           (funcall response-uri-formatter response-uri)))))))
