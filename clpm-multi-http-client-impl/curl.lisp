;;;; Curl integration
;;;;
;;;; This software is part of clpm-multi-http-client. See README.org for more
;;;; information. See LICENSE for license information.

(uiop:define-package #:clpm-multi-http-client-impl/curl
    (:use #:cl
          #:alexandria
          #:flexi-streams
          #:clpm-multi-http-client/defs
          #:clpm-multi-http-client/redirects
          #:clpm-multi-http-client/utils)
  (:import-from #:fast-http)
  (:export #:curl-client))

(in-package #:clpm-multi-http-client-impl/curl)

(defclass curl-client (http-client)
  ((path
    :initarg :path
    :initform "curl"
    :accessor curl-path
    :documentation
    "The path to the curl program.")
   (buffer-size
    :initarg :buffer-size
    :initform 8192
    :accessor curl-buffer-size
    :documentation
    "How many bytes to read at once from curl's output."))
  (:documentation
   "Describes an HTTP client that uses a curl executable."))

(defun headers-to-alist (table)
  (let ((out nil))
    (maphash (lambda (k v)
               (push (cons (make-keyword (uiop:standard-case-symbol-name k)) v)
                     out))
             table)
    out))

(defun process-response-and-get-body-stream (client proc response additional-headers)
  "Given a process info object representing a curl process, an http response
object, an alist of additional headers, return a stream containing the body of
the response.

First prints all additional headers to proc's stdin and closes it. Then, it
begins reading proc's stdout, feeding it into an HTTP parser for the provided
response object. After all the headers are processed, a stream is returned
containing the body of the response. When this stream is CLOSEd, the
corresponding curl process info is cleaned up."
  (let* ((out-stream (uiop:process-info-output proc))
         (in-stream (uiop:process-info-input proc))
         (body-finished nil)
         (result (make-array 0 :element-type '(unsigned-byte 8) :adjustable t))
         (index 0)
         (parser (fast-http:make-parser
                  response
                  :finish-callback
                  (lambda ()
                    (setf body-finished t))
                  :body-callback
                  (lambda (data start end)
                    (adjust-array result (+ index (- end start)))
                    (replace result data :start1 index :start2 start :end2 end)
                    (incf index (- end start))))))
    ;; Write all additional headers to curl's stdin
    (loop
      :for (key . value) :in additional-headers
      :for name := (string-downcase (symbol-name key))
      :do (format in-stream "~A: ~A~%" name value))
    (unless additional-headers
      ;; Curl on Windows seems to get rather upset if the stream is just
      ;; closed without anything being written to it, so just send a
      ;; blank line to make it happy.
      (format in-stream "~%"))
    ;; Close the stream to let curl know we're finished giving it headers.
    (close in-stream)

    ;; Now start pumping the output.
    (loop
      :with buffer := (make-array (curl-buffer-size client) :element-type '(unsigned-byte 8))
      :for pos := (read-sequence buffer out-stream)
      :unless (zerop pos)
        :do (funcall parser buffer :start 0 :end pos)
      :while (and (not (zerop pos))
                  (not body-finished)))
    (close (uiop:process-info-error-output proc))
    (close (uiop:process-info-output proc))
    (close (uiop:process-info-input proc))
    (uiop:wait-process proc)
    result))

(defmethod http-client-request ((client curl-client) uri
                                &rest args
                                &key (method :get)
                                  want-stream (redirect 5) additional-headers
                                &allow-other-keys)
  (let* ((response-uri-formatter (make-response-uri-formatter uri :string))
         (uri (uri-to-string uri))
         (response (fast-http:make-http-response))
         (octets
           (process-response-and-get-body-stream
            client
            (uiop:launch-program
             `(,(curl-path client)
               ,@(ecase method
                   (:get nil)
                   (:head (list "-I")))
               ;; Add the requested headers. Pass them in on stdin to prevent
               ;; them from being visible in the process list (in case any of
               ;; them are authentication headers...)
               "-H" "@-"
               ;; Include headers in the output
               "-i"
               ;; Don't read any user config files
               "-q"
               ;; fast-http wasn't designed for HTTP 2, so force 1.1.
               "--http1.1"
               ;; We're handling the transfer encoding.
               "--raw"
               ;; Be quiet, but still show the error (if one occurs)
               "--silent"
               "--show-error"
               ,uri)
             :output :stream
             :error-output :stream
             :input :stream
             :element-type '(unsingned-byte 8))
            response
            additional-headers))
         (response-headers (headers-to-alist (fast-http:http-headers response)))
         (response-status-code (fast-http:http-status response)))

    (when (redirect-p response-status-code response-headers redirect)
      ;;(close stream)
      (return-from http-client-request
        (apply #'handle-redirect client uri response-status-code response-headers
               :redirect (if (numberp redirect) (1- redirect) redirect)
               args)))

    (return-from http-client-request
      (values
       (if want-stream
           (make-in-memory-input-stream octets)
           octets)
       response-status-code
       response-headers
       (funcall response-uri-formatter uri)))))
