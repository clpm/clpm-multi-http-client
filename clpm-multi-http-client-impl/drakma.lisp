;;;; Drakma integration
;;;;
;;;; This software is part of clpm-multi-http-client. See README.org for more
;;;; information. See LICENSE for license information.

(uiop:define-package #:clpm-multi-http-client-impl/drakma
    (:use #:cl
          #:alexandria
          #:flexi-streams
          #:clpm-multi-http-client/connection-pool
          #:clpm-multi-http-client/defs
          #:clpm-multi-http-client/forwarding-input-stream
          #:clpm-multi-http-client/redirects
          #:clpm-multi-http-client/utils)
  (:import-from #:drakma)
  (:export #:drakma-client))

(in-package #:clpm-multi-http-client-impl/drakma)

(defclass drakma-client (http-client)
  ((pool-manager
    :initform nil
    :accessor http-client-pool-manager)))

(defmethod initialize-instance :after ((client drakma-client)
                                       &key (use-connection-pool t)
                                         num-pools
                                         pool-max-size
                                       &allow-other-keys)
  (when use-connection-pool
    (setf (http-client-pool-manager client)
          (apply #'make-instance
                 'pool-manager
                 (append
                  (when num-pools (list :num-pools num-pools))
                  (when pool-max-size (list :pool-max-size pool-max-size)))))))

(defclass drakma-stream (connection-pool-input-stream)
  ()
  (:documentation
   "Wraps Drakma's flexi stream. When instantiated, sets the flexi-stream's
bound to prevent reads past the body."))

(defmethod initialize-instance :after ((ds drakma-stream)
                                       &key
                                         content-length
                                       &allow-other-keys)
  (when content-length
    (let* ((raw-stream (forwarding-input-stream-stream ds))
           (position (flexi-stream-position raw-stream)))
      (setf (flexi-stream-bound raw-stream) (+ position content-length)))))

(defmethod close ((ds drakma-stream) &key abort)
  (declare (ignore abort))
  (unless (forwarding-input-stream-closed-p ds)
    (when-let ((raw-stream (forwarding-input-stream-stream ds)))
      (let ((position (flexi-stream-position raw-stream))
            (bound (flexi-stream-bound raw-stream)))
        (cond
          ((null bound)
           (setf (flexi-stream-element-type raw-stream) 'octet)
           (loop
             :until (eql :eof (read-byte raw-stream nil :eof))))
          ((< position bound)
           ;; Read until the end...
           (setf (flexi-stream-element-type raw-stream) 'octet)
           (dotimes (j (- bound position))
             (read-byte raw-stream nil))))
        (setf (flexi-stream-bound raw-stream) nil)))
    (call-next-method)))

(defmethod http-client-request ((client drakma-client) uri
                                &rest args
                                &key (method :get) force-binary
                                  want-stream (redirect 5) additional-headers
                                &allow-other-keys)
  (tagbody
   top
     (let* ((response-uri-formatter (make-response-uri-formatter uri :puri))
            (uri (ensure-puri uri))
            (pool-manager (http-client-pool-manager client))
            (pool (when pool-manager (manager-get-pool pool-manager uri)))
            (stream (when pool (connection-pool-grab-stream pool))))
       (flet ((add-stream-to-pool (response-uri response-stream)
                (let ((pool (when pool-manager (manager-get-pool pool-manager response-uri))))
                  (when pool
                    (connection-pool-add-stream pool response-stream)))))
         (handler-bind ((drakma:drakma-error
                          (lambda (c)
                            (when (and stream
                                       (uiop:match-condition-p "No status line - probably network error." c))
                              ;; The stream is no longer valid. Close it and try again.
                              (ignore-errors (close stream))
                              (go top)))))
           (multiple-value-bind
                 (response-body response-status-code response-headers response-uri
                  response-stream response-stream-must-close-p)
               (drakma:http-request
                uri
                :method method
                :stream stream
                :additional-headers additional-headers
                :want-stream want-stream
                :force-binary force-binary
                :redirect nil
                :verify :required
                :close (null pool-manager))

             (unless want-stream
               (cond
                 (response-stream-must-close-p
                  (ignore-errors (close response-stream)))
                 ((add-stream-to-pool response-uri response-stream))
                 (t
                  (ignore-errors (close response-stream)))))

             (when (redirect-p response-status-code response-headers redirect)
               (when want-stream
                 (when (response-has-body-p response-headers)
                   ;; We need to read to the end of the stream before we can
                   ;; return it to the pool.
                   (let ((content-length (assoc-value response-headers :content-length)))
                     (if content-length
                         (dotimes (j (parse-integer content-length))
                           (read-byte response-body))
                         (loop
                           :until (eql :eof (read-byte response-body nil :eof))))))
                 (cond
                   (response-stream-must-close-p
                    (ignore-errors (close response-body))
                    (ignore-errors (close response-stream)))
                   ((add-stream-to-pool response-uri response-stream))
                   (t
                    (ignore-errors (close response-body))
                    (ignore-errors (close response-stream)))))
               (return-from http-client-request
                 (apply #'handle-redirect client uri response-status-code response-headers
                        :redirect (if (numberp redirect) (1- redirect) redirect)
                        args)))
             (let* ((content-length (assoc-value response-headers :content-length))
                    (has-body-p (response-has-body-p response-headers)))
               (return-from http-client-request
                 (values
                  (if want-stream
                      (make-instance 'drakma-stream
                                     :stream response-body
                                     :closedp (not has-body-p)
                                     :content-length (cond
                                                       ((= response-status-code 304)
                                                        0)
                                                       (content-length
                                                        (parse-integer content-length)))
                                     :close-fn (when pool-manager
                                                 (lambda (abort)
                                                   (cond
                                                     (response-stream-must-close-p
                                                      (ignore-errors (close response-body :abort abort))
                                                      (ignore-errors (close response-stream :abort abort)))
                                                     ((add-stream-to-pool response-uri response-stream))
                                                     (t
                                                      (ignore-errors (close response-body :abort abort))
                                                      (ignore-errors (close response-stream :abort abort)))))))
                      response-body)
                  response-status-code
                  response-headers
                  (funcall response-uri-formatter response-uri))))))))))
