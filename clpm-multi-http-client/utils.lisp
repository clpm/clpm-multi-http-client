;;;; Utilities
;;;;
;;;; This software is part of clpm-multi-http-client. See README.org for more
;;;; information. See LICENSE for license information.

(uiop:define-package #:clpm-multi-http-client/utils
    (:use #:cl
          #:alexandria)
  (:import-from #:puri)
  (:import-from #:quri)
  (:export #:ensure-puri
           #:ensure-quri
           #:make-response-uri-formatter
           #:response-has-body-p
           #:uri-to-string))

(in-package #:clpm-multi-http-client/utils)

(defgeneric ensure-puri (uri))

(defmethod ensure-puri ((uri string))
  (puri:parse-uri uri))

(defmethod ensure-puri ((uri puri:uri))
  uri)

(defmethod ensure-puri ((uri quri:uri))
  (puri:parse-uri (quri:render-uri uri)))

(defgeneric ensure-quri (uri))

(defmethod ensure-quri ((uri string))
  (quri:uri uri))

(defmethod ensure-quri ((uri puri:uri))
  (quri:uri (puri:render-uri uri nil)))

(defmethod ensure-quri ((uri quri:uri))
  uri)

(defgeneric make-response-uri-formatter (orig-uri input))

(defmethod make-response-uri-formatter ((orig-uri string) input)
  (ecase input
    (:puri
     (lambda (x) (puri:render-uri x nil)))
    (:quri
     (lambda (x) (quri:render-uri x)))
    (:string
     #'identity)))

(defmethod make-response-uri-formatter ((orig-uri puri:uri) input)
  (ecase input
    (:puri
     #'identity)
    (:quri
     #'ensure-puri)
    (:string
     #'ensure-puri)))

(defmethod make-response-uri-formatter ((orig-uri quri:uri) input)
  (ecase input
    (:puri
     #'ensure-quri)
    (:quri
     #'identity)
    (:string
     #'ensure-quri)))

(defun response-has-body-p (response-headers)
  (let ((content-length (assoc-value response-headers :content-length))
        (transfer-encoding (assoc-value response-headers :transfer-encoding)))
     (or content-length
         (and (not content-length) transfer-encoding))))

(defgeneric uri-to-string (uri))

(defmethod uri-to-string ((uri string))
  uri)

(defmethod uri-to-string ((uri puri:uri))
  (puri:render-uri uri nil))

(defmethod uri-to-string ((uri quri:uri))
  (quri:render-uri uri))
