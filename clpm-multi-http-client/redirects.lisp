;;;; Redirect handling
;;;;
;;;; This software is part of clpm-multi-http-client. See README.org for more
;;;; information. See LICENSE for license information.

(uiop:define-package #:clpm-multi-http-client/redirects
    (:use #:cl
          #:alexandria
          #:clpm-multi-http-client/defs)
  (:import-from #:puri)
  (:import-from #:quri)
  (:export #:+redirect-codes+
           #:+redirect-to-get-codes+
           #:handle-redirect
           #:redirect-p))

(in-package #:clpm-multi-http-client/redirects)

(define-constant +redirect-codes+ '(301 302 303 307) :test 'equal)
(define-constant +redirect-to-get-codes+ '(302 303) :test 'equal)

(defun redirect-p (status-code headers redirect)
  (and (member status-code +redirect-codes+)
       (or (eq redirect t)
           (and (numberp redirect)
                (plusp redirect)))
       (assoc-value headers :location)))

(defgeneric merge-location-uri (location-header uri))

(defmethod merge-location-uri (location-header (uri string))
  (puri:merge-uris location-header (puri:uri uri)))

(defmethod merge-location-uri (location-header (uri puri:uri))
  (puri:merge-uris location-header uri))

(defmethod merge-location-uri (location-header (uri quri:uri))
  (quri:merge-uris location-header uri))

(defun handle-redirect (client orig-uri status-code headers &rest args &key (method :get) &allow-other-keys)
  (when (member status-code +redirect-to-get-codes+)
    (setf method :get))
  (let* ((location (assoc-value headers :location))
         (new-uri (merge-location-uri location orig-uri)))
    (apply #'http-client-request client new-uri :method method args)))
