;;;; Connection pool
;;;;
;;;; This software is part of clpm-multi-http-client. See README.org for more
;;;; information. See LICENSE for license information.

(uiop:define-package #:clpm-multi-http-client/connection-pool
    (:use #:cl
          #:alexandria
          #:clpm-multi-http-client/forwarding-input-stream)
  (:import-from #:bordeaux-threads)
  (:import-from #:puri)
  (:import-from #:quri)
  (:export #:connection-pool-add-stream
           #:connection-pool-grab-stream
           #:connection-pool-input-stream
           #:manager-get-pool
           #:pool-manager))

(in-package #:clpm-multi-http-client/connection-pool)

(defclass pool-manager ()
  ((pool-ht
    :initform (make-hash-table
               :test 'equal)
    :reader pool-manager-pool-ht
    :documentation
    "Maps a two element list of scheme (as keyword) and authority to a
connection pool instance.")
   (lock
    :initform (bt:make-recursive-lock "pool manager")
    :reader pool-manager-lock)
   (ordered-list
    :initform nil
    :accessor pool-manager-ordered-list
    :documentation
    "Used to implement a LRU cache. TODO: Make this be a doubly linked list for
O(1) operations.")
   (num-pools
    :initarg :num-pools
    :initform 10
    :reader pool-manager-num-pools)
   (pool-max-size
    :initarg :pool-max-size
    :initform 5
    :reader pool-manager-pool-max-size))
  (:documentation
   "Used to hold connection pools for a number of "))

(defun manager-get-pool (manager uri)
  (let* ((uri (etypecase uri
                (string (quri:uri uri))
                (puri:uri uri)
                (quri:uri uri)))
         (scheme-and-authority
           (etypecase uri
             (puri:uri (list (puri:uri-scheme uri) (string-downcase (puri:uri-authority uri))))
             (quri:uri (list (quri:uri-scheme uri) (string-downcase (quri:uri-authority uri)))))))
    (with-accessors ((num-pools pool-manager-num-pools)
                     (ht pool-manager-pool-ht)
                     (lock pool-manager-lock)
                     (ordered-list pool-manager-ordered-list)
                     (pool-max-size pool-manager-pool-max-size))
        manager
      ;; Lock ensures that we don't create two connection pools for the same pair.
      (bt:with-recursive-lock-held (lock)
        (multiple-value-bind (pool exists-p) (gethash scheme-and-authority ht)
          (unless exists-p
            (setf pool (make-instance 'connection-pool
                                      :scheme (first scheme-and-authority)
                                      :authority (second scheme-and-authority)
                                      :max-size pool-max-size))
            (setf (gethash scheme-and-authority ht) pool)
            (push pool ordered-list)
            (when (> (length ordered-list)
                     num-pools)
              (let ((pool-to-remove (last-elt ordered-list)))
                (setf ordered-list (butlast ordered-list))
                (connection-pool-destroy pool-to-remove))))
          ;; Place the pool at the front of the list.
          (removef ordered-list pool)
          (push pool ordered-list)
          pool)))))

(defclass connection-pool ()
  ((authority
    :initarg :authority
    :accessor connection-pool-authority)
   (scheme
    :initarg :scheme
    :accessor connection-pool-scheme)
   (max-size
    :initarg :max-size
    :initform 5
    :accessor connection-pool-max-size)
   (lock
    :initform (bt:make-lock "Connection Pool Lock")
    :accessor connection-pool-lock)
   (cv
    :initform (bt:make-condition-variable :name "Pool notifier")
    :accessor connection-pool-cv)
   (storage
    :initform nil
    :accessor connection-pool-storage)))

(defun connection-pool-destroy (pool)
  pool)

(defun connection-pool-size (pool)
  (length (connection-pool-storage pool)))

(defmethod initialize-instance :after ((pool connection-pool) &key max-size)
  (setf (connection-pool-storage pool)
        (make-array max-size :fill-pointer 0)))

(defun connection-pool-add-stream (pool stream)
  (with-accessors ((lock connection-pool-lock)
                   (max-size connection-pool-max-size)
                   (storage connection-pool-storage))
      pool
    (bt:with-lock-held (lock)
      (vector-push stream storage))))

(defun connection-pool-grab-stream (pool &key block)
  (with-accessors ((lock connection-pool-lock)
                   (max-size connection-pool-max-size)
                   (storage connection-pool-storage)
                   (cv connection-pool-cv))
      pool
    (bt:with-lock-held (lock)
      (when block
        (loop
          :while (zerop (connection-pool-size pool))
          :do (bt:condition-wait cv lock)))
      (when (plusp (connection-pool-size pool))
        (vector-pop storage)))))

(defclass connection-pool-input-stream (forwarding-input-stream)
  ((close-fn
    :initarg :close-fn
    :initform nil
    :reader connection-pool-input-stream-close-fn)))

(defmethod close ((stream connection-pool-input-stream) &key abort)
  (multiple-value-prog1
      (call-next-method)
    (with-accessors ((close-fn connection-pool-input-stream-close-fn))
        stream
      (when close-fn
        (funcall close-fn abort)))))
