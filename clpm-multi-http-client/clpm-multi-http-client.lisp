;;;; Clpm-Multi-Http-Client primary system
;;;;
;;;; This software is part of clpm-multi-http-client. See README.org for more
;;;; information. See LICENSE for license information.

(uiop:define-package #:clpm-multi-http-client/clpm-multi-http-client
    (:nicknames #:clpm-multi-http-client)
  (:use #:cl
        #:clpm-multi-http-client/convenience
        #:clpm-multi-http-client/defs)
  (:reexport #:clpm-multi-http-client/convenience
             #:clpm-multi-http-client/defs))

(in-package #:clpm-multi-http-client/clpm-multi-http-client)
