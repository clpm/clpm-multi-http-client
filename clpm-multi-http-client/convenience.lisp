;;;; Convenience functions
;;;;
;;;; This software is part of clpm-multi-http-client. See README.org for more
;;;; information. See LICENSE for license information.

(uiop:define-package #:clpm-multi-http-client/convenience
    (:use #:cl
          #:alexandria
          #:clpm-multi-http-client/defs)
  (:import-from #:log4cl)
  (:export #:http-client-ensure-file-fetched))

(in-package #:clpm-multi-http-client/convenience)

(defun day-name (day-of-week)
  (ecase day-of-week
    (0
     "Mon")
    (1
     "Tue")
    (2
     "Wed")
    (3
     "Thu")
    (4
     "Fri")
    (5
     "Sat")
    (6
     "Sun")))

(defun month-name (month)
  (ecase month
    (1 "Jan")
    (2 "Feb")
    (3 "Mar")
    (4 "Apr")
    (5 "May")
    (6 "Jun")
    (7 "Jul")
    (8 "Aug")
    (9 "Sep")
    (10 "Oct")
    (11 "Nov")
    (12 "Dec")))

(defun universal-time-to-http-date (universal-time)
  "Given a universal-time return the same date as a string suitable for HTTP
headers."
  (multiple-value-bind (second minute hour date month year day-of-week)
      ;; Times in HTTP Headers are always GMT
      (decode-universal-time universal-time 0)
    (format nil "~A, ~2,'0D ~A ~A ~2,'0D:~2,'0D:~2,'0D GMT"
            (day-name day-of-week) date (month-name month) year hour minute second)))

(defun http-client-ensure-file-fetched (client pathname uri &key force hint
                                                              additional-headers)
  "Given a pathname, make sure it exists. If it does not exist, fetch it from
URI. If it does exist, the behavior is dependent on the FORCE and HINT flags:

If FORCE is non-NIL, the file is fetched and overwritten.

If HINT is:

+ :NIL :: If the file is present, a GET request is sent with a If-Modified-Since
header set to the write date of the existing file. If the server replies with a
304, the local file is not touched. If it replies with a 200, the local file is
overwritten.

+ :IMMUTABLE :: If the destination file is present, no HTTP requests are sent at
all.

Returns T if the contents of PATHNAME were modified, NIL otherwise."
  (unless (and (eql hint :immutable)
               (probe-file pathname))
    (log:debug "Fetching ~A" uri)

    (when (and (not force)
               (probe-file pathname))
      (push (cons :if-modified-since (universal-time-to-http-date (file-write-date pathname)))
            additional-headers))

    (multiple-value-bind (http-stream status-code)
        (http-client-request client uri
                             :want-stream t
                             :force-binary t
                             :additional-headers additional-headers)
      (unwind-protect
           (progn
             (log:debug "Status code: ~S" status-code)
             (case status-code
               (200
                (ensure-directories-exist pathname)
                (uiop:with-staging-pathname (pathname)
                  (with-open-file (file-stream pathname
                                               :direction :output
                                               :element-type '(unsigned-byte 8)
                                               :if-exists :supersede)
                    (copy-stream http-stream file-stream
                                 :element-type '(unsigned-byte 8))
                    t)))
               (304
                ;; No changes
                nil)
               (t
                (error "Can't handle HTTP code ~A" status-code))))
        (close http-stream)))))
