;;;; Clpm-Multi-Http-Client definitions
;;;;
;;;; This software is part of clpm-multi-http-client. See README.org for more
;;;; information. See LICENSE for license information.

(uiop:define-package #:clpm-multi-http-client/defs
    (:use #:cl)
  (:export #:*default-http-client*
           #:http-client
           #:http-client-request
           #:http-request))

(in-package #:clpm-multi-http-client/defs)

(defvar *default-http-client*)

(defclass http-client ()
  ())

(defgeneric http-client-request (client url
                                 &key method force-binary want-stream
                                   redirect additional-headers
                                 &allow-other-keys))

(defun http-request (url &rest args
                     &key
                       method force-binary want-stream
                       redirect additional-headers
                     &allow-other-keys)
  (declare (ignore method force-binary want-stream redirect additional-headers))
  (apply #'http-client-request *default-http-client* url args))
