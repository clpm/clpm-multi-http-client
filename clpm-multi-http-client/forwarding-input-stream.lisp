;;;; Forwarding input stream
;;;;
;;;; This software is part of clpm-multi-http-client. See README.org for more
;;;; information. See LICENSE for license information.

(uiop:define-package #:clpm-multi-http-client/forwarding-input-stream
    (:use #:cl
          #:trivial-gray-streams)
  (:export #:forwarding-input-stream
           #:forwarding-input-stream-closed-p
           #:forwarding-input-stream-stream))

(in-package #:clpm-multi-http-client/forwarding-input-stream)

(defclass forwarding-input-stream (fundamental-binary-input-stream
                                   fundamental-character-input-stream)
  ((closedp
    :initarg :closedp
    :initform nil
    :accessor forwarding-input-stream-closed-p)
   (stream
    :initarg :stream
    :reader forwarding-input-stream-stream))
  (:documentation
   "An input stream that completely wraps another stream. All input stream
generic functions are simply forwarded to the underlying stream, except for
CLOSE."))

(defmethod close ((stream forwarding-input-stream) &key abort)
  (declare (ignore abort))
  (prog1 (not (forwarding-input-stream-closed-p stream))
    (setf (forwarding-input-stream-closed-p stream) t)))

(defmethod open-stream-p ((stream forwarding-input-stream))
  (unless (forwarding-input-stream-closed-p stream)
    (open-stream-p (forwarding-input-stream-stream stream))))

(defmethod stream-element-type ((stream forwarding-input-stream))
  (stream-element-type (forwarding-input-stream-stream stream)))

(defmethod stream-file-position ((stream forwarding-input-stream))
  (unless (forwarding-input-stream-closed-p stream)
    (stream-file-position (forwarding-input-stream-stream stream))))

(defmethod (setf stream-file-position) (position-spec (stream forwarding-input-stream))
  (unless (forwarding-input-stream-closed-p stream)
    (setf (stream-file-position (forwarding-input-stream-stream stream)) position-spec)))

(defmethod stream-clear-input ((stream forwarding-input-stream))
  (unless (forwarding-input-stream-closed-p stream)
    (stream-clear-input (forwarding-input-stream-stream stream))))

(defmethod stream-listen ((stream forwarding-input-stream))
  (unless (forwarding-input-stream-closed-p stream)
    (stream-listen (forwarding-input-stream-stream stream))))

(defmethod stream-peek-char ((stream forwarding-input-stream))
  (if (forwarding-input-stream-closed-p stream)
      :eof
      (stream-peek-char (forwarding-input-stream-stream stream))))

(defmethod stream-read-byte ((stream forwarding-input-stream))
  (if (forwarding-input-stream-closed-p stream)
      :eof
      (stream-read-byte (forwarding-input-stream-stream stream))))

(defmethod stream-read-char ((stream forwarding-input-stream))
  (if (forwarding-input-stream-closed-p stream)
      :eof
      (stream-read-char (forwarding-input-stream-stream stream))))

(defmethod stream-read-char-no-hang ((stream forwarding-input-stream))
  (if (forwarding-input-stream-closed-p stream)
      :eof
      (stream-read-char-no-hang (forwarding-input-stream-stream stream))))

(defmethod stream-read-line ((stream forwarding-input-stream))
  (if (forwarding-input-stream-closed-p stream)
      :eof
      (stream-read-line (forwarding-input-stream-stream stream))))

(defmethod stream-read-sequence ((stream forwarding-input-stream) sequence start end
                                 &rest args
                                 &key &allow-other-keys)
  (if (forwarding-input-stream-closed-p stream)
      start
      (apply #'read-sequence sequence
             (forwarding-input-stream-stream stream)
             :start start :end end
             args)))

(defmethod stream-unread-char ((stream forwarding-input-stream) char)
  (unless (forwarding-input-stream-closed-p stream)
    (stream-unread-char (forwarding-input-stream-stream stream) char)))
