;;;; Clpm-Multi-Http-Client System Definition
;;;;
;;;; This software is part of clpm-multi-http-client. See README.org for more
;;;; information. See LICENSE for license information.

#-:asdf3.2
(error "Requires ASDF >=3.2")

;; Not necessary, but easier to have when using SLIME.
(in-package :asdf-user)

(defsystem #:clpm-multi-http-client
  :version "0.0.1"
  :description "A library to provide a uniform interface over several HTTP clients."
  :license "BSD-2-Clause"
  :pathname "clpm-multi-http-client/"
  :class :package-inferred-system
  :depends-on (#:clpm-multi-http-client/clpm-multi-http-client))
